import { combineReducers } from "redux";

import ThemeReducer from "./ThemeReducer";
import DepartmentReducer from "./DepartmentReducer";

export default combineReducers({
  themes: ThemeReducer,
  appData: DepartmentReducer,
});
