import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { withTheme } from "@material-ui/core";
import { connect } from "react-redux";
import "./OAuth.css";
import Header from "../Home/Header";
import { Button } from "@material-ui/core";
class OAuth extends Component {
  render() {
    const styles = {
      button: {
        background: this.props.getTheme.palette.secondary.main,
        margin: "1rem 1.5rem",
      },
    };
    return (
      <React.Fragment>
        <Header history={this.props.history} location={this.props.location} />
        <div
          style={{
            position: "absolute",
            left: "50%",
            top: "50%",
            transform: "translate(-50%,-50%)",
          }}
        >
          <Button
            style={styles.button}
            onClick={() => this.props.history.push("/switchon/signup")}
          >
            Continue with Email
          </Button>
          <p>
            Already have an account?{" "}
            <NavLink id="Login-nav" to="login">
              Login
            </NavLink>
          </p>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    getTheme: state.themes.theme,
  };
};
export default connect(mapStateToProps, null)(withTheme()(OAuth));
