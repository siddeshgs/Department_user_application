import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";

class EmailHeader extends Component {
  state = {};
  render() {
    return (
      <div className="EmailHeader">
        <AppBar position="static" color="default">
          <Toolbar>
            <img
              src={"https://i.ibb.co/Fb2btSb/Eve-Hub-logo.png"}
              alt="logo of Eve HUb"
            />{" "}
            <NavLink to="/switchon/">Home</NavLink>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default EmailHeader;
