import { io } from "socket.io-client";
import ENV from "./environment";

const ENDPOINT = ENV.API_URL; // local backend

// const ENDPOINT = "http://13.233.97.176:5000";
// const ENDPOINT = "http://localhost:5000";

let socket = io(ENDPOINT);

export default socket;
