const router = require("./router");
const forms = require("../controllers/formsController");

// user Routes
router.post("/api/switchon/create_form", forms.createForm);
router.post(
  "/api/switchon/get_pending_request_of_department",
  forms.getPendingRequest
);
router.post(
  "/api/switchon/get_approved_request_of_department",
  forms.getApprovedRequest
);
router.post(
  "/api/switchon/get_rejected_request_of_department",
  forms.getRejectedRequest
);
router.post(
  "/api/switchon/get_request_for_approval",
  forms.getRequestForApproval
);
router.post(
  "/api/switchon/update_and_approval_request",
  forms.updateAndApprovalRequest
);
router.post(
  "/api/switchon/update_and_reject_request",
  forms.updateAndRejectRequest
);

module.exports = router;
