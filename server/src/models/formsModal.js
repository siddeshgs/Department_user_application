const mongoose = require("mongoose");
const joi = require("joi");
const Schema = mongoose.Schema;

const formsSchema = new Schema({
  status: { type: Schema.Types.String },
  departmentId: { type: Schema.Types.ObjectId, ref: "departmentModel" },
  created_by: { type: Schema.Types.ObjectId, ref: "UserRegistrationModel" },
  user: { type: Schema.Types.ObjectId, ref: "UserRegistrationModel" },
  message: { type: Schema.Types.String },
  created_at: Date,
});

module.exports.formsModal = mongoose.model("formsModal", formsSchema);

// module.exports.departmentValidate = joi.object().keys({
//   departmentName: joi.string().required(),
// });
