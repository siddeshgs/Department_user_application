import {
  Avatar,
  Button,
  Card,
  CardHeader,
  Typography,
} from "@material-ui/core";
import React, { Component } from "react";
import { connect } from "react-redux";
import { get_pending_request_of_department_action } from "../../Redux/ActionCreators/DepartmentAction";

const styles = {
  head: {
    width: "100%",
    color: "black",
    fontSize: "2rem",
    backgroundColor: "#acd8c4",
    padding: "1rem 0",
  },
};

class PendingForm extends Component {
  state = {
    name: "",
  };

  componentDidMount() {
    this.props.get_pending_request_of_department_action(
      localStorage.getItem("departmentId")
    );

    this.setState({ name: localStorage.getItem("username") });
  }

  render() {
    console.log(this.props);
    // const { name, department, user } = this.state;
    return (
      <>
        <div style={styles.head}>Pending Request</div>
        {this.props.appData.pending_request.length === 0 && (
          <div style={{ marginTop: "3rem" }}>No Request found</div>
        )}
        {this.props.appData.pending_request.map((req) => {
          return (
            <Card
              style={{
                margin: "8px",
                minHeight: "3rem",
              }}
              key={req._id}
            >
              <CardHeader
                avatar={
                  <Avatar aria-label="recipe">
                    {req.created_by.username.slice(0, 3)}
                  </Avatar>
                }
                action={
                  <div style={{ alignContent: "center", marginTop: "10px" }}>
                    <div
                      color="inherit"
                      style={{
                        width: "6rem",
                        backgroundColor: "rgb(251 223 89 / 81%)",
                        padding: "8px 1rem",
                        borderRadius: "10px",
                      }}
                    >
                      pending
                    </div>
                  </div>
                }
                title={
                  <Typography
                    style={{
                      whiteSpace: "nowrap",
                      overflow: "hidden",
                      textOverflow: "ellipsis",
                      width: "12rem",
                    }}
                  >
                    {req.message}
                  </Typography>
                }
              />
            </Card>
          );
        })}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    appData: state.appData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    get_pending_request_of_department_action: (department_id) =>
      dispatch(get_pending_request_of_department_action(department_id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PendingForm);
