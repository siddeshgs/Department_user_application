const router = require("./router");
const department = require("../controllers/departmentController");

// user Routes
router.post(
  "/api/switchon/get_department_and_users",
  department.get_department_and_users
);
router.post("/api/switchon/create_department", department.create_department);

module.exports = router;
