import { SET_THEME } from "../ActionCreators/ActionTypes";


export default (state = {}, action) => {
  switch (action.type) {
    case SET_THEME:
      return {
        ...state,
        theme: action.theme
      };
    default:
      return state;
  }
};
