import axios from "axios";
import {
  GET_DEPARTMENT_AND_USERS,
  GET_PENDING_REQUEST_OF_DEPARTMENT,
  GET_APPROVED_REQUEST_OF_DEPARTMENT,
  GET_REJECTED_REQUEST_OF_DEPARTMENT,
  REQUEST_FOR_APPROVAL,
} from "./ActionTypes";
import ENV from "../../environment";

export const get_department_and_users_action = () => (dispatch) => {
  const url = `${ENV.API_URL}/api/switchon/get_department_and_users`;
  axios
    .post(url)
    .then((res) => {
      //   console.log(res);
      dispatch({
        type: GET_DEPARTMENT_AND_USERS,
        department_data: res.data.department_data,
        userRegistration_data: res.data.userRegistration_data,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

export const create_form_action = (data) => (dispatch) => {
  const url = `${ENV.API_URL}/api/switchon/create_form`;
  axios
    .post(url, data)
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

export const get_pending_request_of_department_action = (department_id) => (
  dispatch
) => {
  const url = `${ENV.API_URL}/api/switchon/get_pending_request_of_department`;
  axios
    .post(url, { department_id })
    .then((res) => {
      // console.log(res);
      dispatch({
        type: GET_PENDING_REQUEST_OF_DEPARTMENT,
        payload: res.data.pending_request,
      });
    })
    .catch((err) => console.log(err));
};

export const get_approved_request_of_department_action = (department_id) => (
  dispatch
) => {
  const url = `${ENV.API_URL}/api/switchon/get_approved_request_of_department`;
  axios
    .post(url, { department_id })
    .then((res) => {
      dispatch({
        type: GET_APPROVED_REQUEST_OF_DEPARTMENT,
        payload: res.data.approved_request,
      });
    })
    .catch((err) => console.log(err));
};

export const get_rejected_request_of_department_action = (department_id) => (
  dispatch
) => {
  const url = `${ENV.API_URL}/api/switchon/get_rejected_request_of_department`;
  axios
    .post(url, { department_id })
    .then((res) => {
      dispatch({
        type: GET_REJECTED_REQUEST_OF_DEPARTMENT,
        payload: res.data.rejected_request,
      });
    })
    .catch((err) => console.log(err));
};

export const get_request_for_approval_action = (department_id, userId) => (
  dispatch
) => {
  const url = `${ENV.API_URL}/api/switchon/get_request_for_approval`;
  axios
    .post(url, { department_id, userId })
    .then((res) => {
      dispatch({
        type: REQUEST_FOR_APPROVAL,
        payload: res.data.request_for_approval,
      });
    })
    .catch((err) => console.log(err));
};

export const update_and_approval_request_action = (formId) => (dispatch) => {
  const url = `${ENV.API_URL}/api/switchon/update_and_approval_request`;
  axios
    .post(url, { formId })
    .then((res) =>
      dispatch({
        type: REQUEST_FOR_APPROVAL,
        payload: res.data.request_for_approval,
      })
    )
    .catch((err) => {
      console.log(err);
    });
};

export const update_and_reject_request_action = (formId) => (dispatch) => {
  const url = `${ENV.API_URL}/api/switchon/update_and_reject_request`;
  axios
    .post(url, { formId })
    .then((res) =>
      dispatch({
        type: REQUEST_FOR_APPROVAL,
        payload: res.data.request_for_approval,
      })
    )
    .catch((err) => {
      console.log(err);
    });
};
