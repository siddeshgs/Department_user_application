import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import React from "react"


const styles = theme => ({
    
    progress: {
      margin: theme.spacing.unit * 2,
      color: '#00695c',
    },
  });

const Loader = props => {
    const { classes } = props;
    return (
        <CircularProgress className={classes.progress} size={30} thickness={5} />
    )
}

  export default withStyles(styles)(Loader);