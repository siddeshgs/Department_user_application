import React, { Component } from "react";
import { NavLink } from "react-router-dom";
class LoginHeader extends Component {
  state = {};
  render() {
    return (
      <div className="LoginHeader">
        <h2>Welcome back.</h2>
        <NavLink id="Navlink" to="/switchon/">
          Home
        </NavLink>
        <p>
          Login to get personalized events, get events and topics you love, and
          interact with events.
        </p>
      </div>
    );
  }
}

export default LoginHeader;
