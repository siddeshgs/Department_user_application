const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const http = require("http");
const { auth } = require("./tokenValidation");
require("dotenv").config({ path: "src/.env" });
const socketIo = require("socket.io");

const app = express();
let server = http.createServer(app);
let io = socketIo(server, {
  cors: {
    origin: "*",
  },
});

let mongoDB = require("./dev").localMongoDB; // local mongodb connection
// let mongoDB = require("./dev").mongoCompassUri;      // mongodb ATLAS connection

// let mongoDB = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/department_user_apply`;
mongoose.connect(mongoDB, { useNewUrlParser: true });

let db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));

// io to res
app.use(function (req, res, next) {
  res.io = io;
  next();
});
app.use(cors());

app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(bodyParser.json({ limit: "50mb" }));

// io.on("connection", (socket) => {
//   console.log("a new user connected");

//   socket.on("new request", ({ departmentId, userId }) => {
//     console.log(departmentId, userId);
//   });
//   // socket.on("messageChange", function (data) {
//   //   console.log(data);
//   //   socket.emit("receive", data.message.split("").reverse().join(""));
//   // });
//   socket.on("disconnect", () => {
//     console.log("disconnect");
//   });
// });

const userRegistrationRoutes = require("./routes/userRegistrationRoutes");
const departmentRoutes = require("./routes/departmentRoutes");
const formsRoutes = require("./routes/formsRoutes");

app.use(userRegistrationRoutes);
app.use(departmentRoutes);
app.use(formsRoutes);

app.use(function (err, req, res, next) {
  console.error(err.message); // Log error message in our server's console
  if (!err.statusCode) err.statusCode = 500; // If err has no specified error code, set error code to 'Internal Server Error (500)'

  res.status(err.statusCode).send(err.message); // All HTTP requests must have a response, so let's send back an error with its status code and message
  next();
  console.log("error sections da");
});

server.listen(process.env.SERVER_PORT, () => {
  console.log(
    "department-user appli RESTful API server started on: " +
      process.env.SERVER_PORT
  );
});
