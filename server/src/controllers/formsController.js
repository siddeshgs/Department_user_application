const joi = require("joi");
const { formsModal } = require("../models/formsModal");
const { UserRegistrationModel } = require("../models/userRegistration");

const CONSTANTS = {
  PENDING: "PENDING",
  APPROVED: "APPROVED",
  REJECTED: "REJECTED",
};

exports.createForm = async (req, res, next) => {
  try {
    const data = req.body;
    console.log("created form ", data);
    const formDetails = await new formsModal({
      departmentId: data.departmentId,
      created_by: data.created_by,
      user: data.user,
      message: data.message,
      status: CONSTANTS.PENDING,
    }).save();

    const user = await UserRegistrationModel.find({ _id: formDetails.user });

    res.io.on("connection", (socket) => {
      console.log("form creation time");

      socket.join(formDetails.departmentId);

      socket
        .to(formDetails.departmentId)
        .emit("new request form", { formDetails, user });

      socket.on("disconnect", () => {
        console.log("disconnect");
      });
    });

    res.send({ formDetails }).status(200);
  } catch (error) {
    next(error);
  }
};

exports.getPendingRequest = async (req, res, next) => {
  try {
    const reqData = req.body;
    console.log(reqData);
    const details = await formsModal
      .find({
        departmentId: reqData.department_id,
        status: CONSTANTS.PENDING,
      })
      .select("-__v")
      .populate("created_by", "-password -__v")
      .exec(function (err, data) {
        if (err) console.log(err);
        console.log(data);
        res.send({ pending_request: data }).status(200);
      });
  } catch (error) {
    next(error);
  }
};

exports.getApprovedRequest = async (req, res, next) => {
  try {
    const reqData = req.body;
    const details = await formsModal
      .find({
        departmentId: reqData.department_id,
        status: CONSTANTS.APPROVED,
      })
      .select("-__v")
      .populate("created_by", "-password -__v")
      .exec(function (err, data) {
        if (err) console.log(err);
        res.send({ approved_request: data }).status(200);
      });
  } catch (error) {
    next(error);
  }
};

exports.getRejectedRequest = async (req, res, next) => {
  try {
    const reqData = req.body;
    const details = await formsModal
      .find({
        departmentId: reqData.department_id,
        status: CONSTANTS.REJECTED,
      })
      .select("-__v")
      .populate("created_by", "-password -__v")
      .exec(function (err, data) {
        if (err) console.log(err);
        res.send({ rejected_request: data }).status(200);
      });
  } catch (error) {
    next(error);
  }
};

exports.getRequestForApproval = async (req, res, next) => {
  try {
    const reqData = req.body;
    const details = await formsModal
      .find({
        departmentId: reqData.department_id,
        user: reqData.userId,
        status: CONSTANTS.PENDING,
      })
      .select("-__v")
      .populate("created_by", "-password -__v")
      .exec(function (err, data) {
        if (err) console.log(err);
        res.send({ request_for_approval: data }).status(200);
      });
  } catch (error) {
    next(error);
  }
};

exports.updateAndApprovalRequest = async (req, res, next) => {
  try {
    const reqData = req.body;
    const details = await formsModal.findByIdAndUpdate(reqData.formId, {
      status: CONSTANTS.APPROVED,
    });
    // if (details.status === CONSTANTS.APPROVED) {
    await formsModal
      .find({
        departmentId: details.departmentId,
        user: details.user,
        status: CONSTANTS.PENDING,
      })
      .select("-__v")
      .populate("created_by", "-password -__v")
      .exec(function (err, data) {
        if (err) console.log(err);
        res.send({ request_for_approval: data }).status(200);
      });
    // }
  } catch (error) {
    next(error);
  }
};

exports.updateAndRejectRequest = async (req, res, next) => {
  try {
    const reqData = req.body;
    const details = await formsModal.findByIdAndUpdate(reqData.formId, {
      status: CONSTANTS.REJECTED,
    });
    // if (details.status === CONSTANTS.APPROVED) {
    await formsModal
      .find({
        departmentId: details.departmentId,
        user: details.user,
        status: CONSTANTS.PENDING,
      })
      .select("-__v")
      .populate("created_by", "-password -__v")
      .exec(function (err, data) {
        if (err) console.log(err);
        res.send({ request_for_approval: data }).status(200);
      });
    // }
  } catch (error) {
    next(error);
  }
};
