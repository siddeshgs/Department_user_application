import {
  GET_APPROVED_REQUEST_OF_DEPARTMENT,
  GET_DEPARTMENT_AND_USERS,
  GET_PENDING_REQUEST_OF_DEPARTMENT,
  GET_REJECTED_REQUEST_OF_DEPARTMENT,
  REQUEST_FOR_APPROVAL,
} from "../ActionCreators/ActionTypes";

const initialState = {
  department_data: [],
  userRegistration_data: [],
  pending_request: [],
  approved_request: [],
  rejected_request: [],
  request_for_approval: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_DEPARTMENT_AND_USERS:
      return {
        ...state,
        department_data: action.department_data,
        userRegistration_data: action.userRegistration_data,
      };

    case GET_PENDING_REQUEST_OF_DEPARTMENT:
      return {
        ...state,
        pending_request: action.payload,
      };

    case GET_APPROVED_REQUEST_OF_DEPARTMENT:
      return {
        ...state,
        approved_request: action.payload,
      };

    case GET_REJECTED_REQUEST_OF_DEPARTMENT:
      return {
        ...state,
        rejected_request: action.payload,
      };
    case REQUEST_FOR_APPROVAL:
      return {
        ...state,
        request_for_approval: action.payload,
      };

    default:
      return state;
  }
};
