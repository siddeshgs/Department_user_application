import {
  Avatar,
  Button,
  Card,
  CardHeader,
  Typography,
} from "@material-ui/core";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  get_request_for_approval_action,
  update_and_approval_request_action,
  update_and_reject_request_action,
} from "../../Redux/ActionCreators/DepartmentAction";

const styles = {
  head: {
    width: "100%",
    color: "black",
    fontSize: "2rem",
    backgroundColor: "#acd8c4",
    padding: "1rem 0",
  },
};

class RequestForApprovalForm extends Component {
  state = {
    name: "",
  };

  componentDidMount() {
    this.props.get_request_for_approval_action(
      localStorage.getItem("departmentId"),
      localStorage.getItem("userId")
    );
    this.setState({ name: localStorage.getItem("username") });
  }

  handleApproval = (event) => {
    console.log(event.currentTarget.value);
    this.props.update_and_approval_request_action(event.currentTarget.value);
  };

  handleReject = (event) => {
    console.log(event.currentTarget.value);
    this.props.update_and_reject_request_action(event.currentTarget.value);
  };

  render() {
    console.log(this.props);
    // const { name, department, user } = this.state;
    return (
      <>
        <div style={styles.head}>Request</div>
        {this.props.appData.request_for_approval.length === 0 && (
          <div style={{ marginTop: "3rem" }}>No Request found</div>
        )}
        {this.props.appData.request_for_approval.map((req) => {
          return (
            <Card
              style={{
                margin: "8px",
                minHeight: "3rem",
              }}
              key={req._id}
            >
              <CardHeader
                avatar={
                  <Avatar aria-label="recipe">
                    {req.created_by.username.slice(0, 3)}
                  </Avatar>
                }
                action={
                  <div style={{ alignContent: "center", marginTop: "8px" }}>
                    <Button
                      color="inherit"
                      variant="outlined"
                      style={{
                        width: "6rem",
                        backgroundColor: "#acd8c4",
                        marginRight: "8px",
                      }}
                      value={req._id}
                      onClick={this.handleApproval}
                    >
                      Approve
                    </Button>
                    <Button
                      color="inherit"
                      variant="outlined"
                      style={{
                        width: "6rem",
                        backgroundColor: "#acd8c4",
                      }}
                      value={req._id}
                      onClick={this.handleReject}
                    >
                      Reject
                    </Button>
                  </div>
                }
                title={
                  <Typography
                    style={{
                      whiteSpace: "nowrap",
                      overflow: "hidden",
                      textOverflow: "ellipsis",
                      width: "12rem",
                    }}
                  >
                    {req.message}
                  </Typography>
                }
              />
            </Card>
          );
        })}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    appData: state.appData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    get_request_for_approval_action: (department_id, userId) =>
      dispatch(get_request_for_approval_action(department_id, userId)),
    update_and_approval_request_action: (formId) =>
      dispatch(update_and_approval_request_action(formId)),
    update_and_reject_request_action: (formId) =>
      dispatch(update_and_reject_request_action(formId)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestForApprovalForm);
