const { departmentModel, departmentValidate } = require("../models/department");
const { UserRegistrationModel } = require("../models/userRegistration");
const joi = require("joi");

exports.create_department = async function (req, res, next) {
  try {
    console.log("create department");
    const data = req.body;
    const departmentNameCheck = await UserRegistrationModel.findOne({
      username: data.departmentName,
    });
    if (departmentNameCheck !== null) {
      throw { message: "username already exits", statusCode: "400" };
    }

    // joi validation
    const result = Joi.validate(data, departmentValidate);
    if (result.error !== null) {
      throw new Error(`Validation error occured ${result.error}`);
    }

    console.log(data);

    // // saving data
    // let departmentDetails = await new departmentModel({
    //   departmentName: data.departmentName,
    // }).save();

    res.send(departmentDetails).status("200");
  } catch (err) {
    next(err);
  }
};

exports.get_department_and_users = async function (req, res, next) {
  // console.log("in get_department_and_users", req);
  try {
    const department_data = await departmentModel.find({});
    const userRegistration_data = await UserRegistrationModel.find({});

    res.send({ department_data, userRegistration_data }).status(200);
  } catch (error) {
    next(error);
  }
};
