import React, { Component } from "react";
import {
  Input,
  withTheme,
  Button,
  Typography,
  Card,
  CardContent,
  Paper,
  CardHeader,
} from "@material-ui/core";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import Header from "../Home/Header";
import { InputLabel } from "@material-ui/core";
import ENV from "../../environment";

class Email extends Component {
  state = {
    email: "",
    username: "",
    password: "",
    userId: null,
    status: null,
    signedUp: false,
    department_id: "",
  };

  handleEmailChange = (value) => {
    this.setState({
      email: value,
    });
  };

  handleUserChange = (value) => {
    this.setState({
      username: value,
    });
  };

  handlePasswordChange = (value) => {
    this.setState({
      password: value,
    });
  };

  handleSubmit = (e, username, email, password) => {
    // console.log(username, email, password);
    // console.log(username, email, password);
    e.preventDefault();
    const url = `${ENV.API_URL}/api/switchon/signup`;
    axios
      .post(url, {
        username,
        email,
        password,
      })
      .then((resp) => {
        // console.log(resp);
        this.setState(
          {
            userId: resp.data.userId,
            status: resp.status,
            signedUp: true,
            department_id: resp.data.department_id,
          },
          () => console.log(this.state)
        );
      })
      .catch((err) => console.error(err));
  };
  render() {
    if (this.state.signedUp) {
      localStorage.setItem("email", this.state.email);
      localStorage.setItem("userId", this.state.userId);
      localStorage.setItem("departmentId", this.state.department_id);
      localStorage.setItem("username", this.state.username);
      return <Redirect to={`/switchon/home/${this.state.username}`} />;
    }

    const styles = {
      button: {
        background: this.props.getTheme.palette.secondary.main,
        width: "10rem",
        margin: "1rem 7rem",
      },
      input: {
        display: "flex",
        flexDirection: "column",
        margin: "1rem 2rem",
      },
      inputLabel: {
        display: "flex",
        flexDirection: "column",
        margin: "1rem 2rem",
        marginTop: "3rem",
        alignContent: "space-between",
      },
      userLabel: {
        color: this.props.getTheme.palette.secondary.main,
        margin: "1rem 3rem",
      },
    };

    return (
      <div>
        <Header history={this.props.history} location={this.props.location} />
        <Card
          style={{
            width: "25rem",
            position: "absolute",
            background: "rgb(256,256,256,0.8)",
            left: "50%",
            top: "60%",
            transform: "translate(-50%,-50%)",
          }}
        >
          <CardHeader
            style={{ background: this.props.getTheme.palette.primary.main }}
            title={
              <Typography variant="h4" style={styles.userLabel}>
                User Registration
              </Typography>
            }
          />
          <CardContent>
            <form
              onSubmit={(e) =>
                this.handleSubmit(
                  e,
                  this.state.username,
                  this.state.email,
                  this.state.password
                )
              }
              method="POST"
              className="Form-data"
            >
              <div style={{ display: "flex" }}>
                <div style={styles.inputLabel}>
                  <InputLabel style={{ margin: "1rem" }}>Email</InputLabel>
                  <InputLabel style={{ margin: "1rem" }}>Username</InputLabel>
                  <InputLabel style={{ margin: "1rem" }}>Password</InputLabel>
                </div>
                <div style={styles.input}>
                  <Input
                    style={{ marginTop: "2rem" }}
                    onChange={(e) => this.handleEmailChange(e.target.value)}
                  />
                  <Input
                    style={{ marginTop: "1rem" }}
                    onChange={(e) => this.handleUserChange(e.target.value)}
                  />
                  <Input
                    style={{ marginTop: "1rem" }}
                    type="password"
                    onChange={(e) => this.handlePasswordChange(e.target.value)}
                  />
                </div>
              </div>

              <Button
                onClick={(e) =>
                  this.handleSubmit(
                    e,
                    this.state.username,
                    this.state.email,
                    this.state.password
                  )
                }
                // onClick={e => this.props.history.push("/switchon/login")}
                style={styles.button}
              >
                Submit
              </Button>
            </form>
          </CardContent>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    getTheme: state.themes.theme,
  };
};
export default connect(mapStateToProps, null)(withTheme()(Email));
