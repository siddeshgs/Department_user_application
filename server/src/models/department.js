const mongoose = require("mongoose");
const joi = require("joi");
const Schema = mongoose.Schema;

const departmentSchema = new Schema({
  departmentName: { type: String },
  // users: [{ type: Schema.Types.ObjectId, ref: "UserRegistrationModel" }],
  created_at: Date,
});

module.exports.departmentModel = mongoose.model(
  "departmentModel",
  departmentSchema
);

module.exports.departmentValidate = joi.object().keys({
  departmentName: joi.string().required(),
});
