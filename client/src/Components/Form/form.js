import React, { Component } from "react";
import { connect } from "react-redux";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { Button, Grid, TextField } from "@material-ui/core";
import {
  get_department_and_users_action,
  create_form_action,
} from "../../Redux/ActionCreators/DepartmentAction";

const styles = {
  head: {
    width: "100%",
    color: "black",
    fontSize: "2rem",
    backgroundColor: "#acd8c4",
    padding: "1rem 0",
  },
  grid: {
    margin: "2rem",
  },
  TextField: {
    width: "80%",
    maxHeight: "8rem",
  },
};

class FormComponent extends Component {
  state = {
    name: "",
    department: "",
    user: "",
    userArray: [],
    message: "",
  };

  componentDidMount() {
    this.props.get_department_and_users_action();

    this.setState({ name: localStorage.getItem("username") });
  }

  handleChange = (event) => {
    console.log(event);
    const usrAry = this.props.appData.userRegistration_data.reduce(
      (store, usr) => {
        if (
          usr.department_id === event.target.value &&
          usr._id !== localStorage.getItem("userId")
        ) {
          store.push(usr);
        }
        return store;
      },
      []
    );
    this.setState({ department: event.target.value, userArray: usrAry });
  };

  handleChangeUser = (event) => {
    if (this.state.department !== "") {
      this.setState({ user: event.target.value });
    } else {
      alert("select department first");
    }
  };

  handleSubmit = () => {
    let userId = "";
    this.props.appData.userRegistration_data.map((usr) => {
      if (usr.username === this.state.user) userId = usr._id;
      return usr;
    });
    const data = {
      created_by: localStorage.getItem("userId"),
      departmentId: this.state.department,
      user: userId,
      message: this.state.message,
    };

    if (this.state.user !== "") {
      this.props.create_form_action(data);
      this.setState({
        department: "",
        user: "",
        message: "",
      });
      // socket.emit("new request", {
      //   departmentId: this.state.department,
      //   userId: userId,
      // });
      // socket.emit("disconnect");
      // socket.off();
    }
  };

  handleTextChange = (event) => {
    this.setState({ message: event.target.value });
  };

  render() {
    // console.log(this.props);
    const { name, department, user } = this.state;
    return (
      <>
        <div style={styles.head}>Form</div>
        <Grid
          container
          spacing={3}
          justify="space-around"
          alignItems="flex-start"
          style={styles.grid}
        >
          <div
            style={{ width: "30%", textAlign: "start", paddingLeft: "2rem" }}
          >
            <InputLabel id="demo-simple-select-label">Created By</InputLabel>
          </div>
          <div
            style={{ width: "30%", textAlign: "start", paddingLeft: "2rem" }}
          >
            <InputLabel id="demo-simple-select-label">{name}</InputLabel>
          </div>
        </Grid>

        <Grid
          container
          spacing={3}
          justify="space-around"
          alignItems="flex-start"
          style={styles.grid}
        >
          <div
            style={{ width: "30%", textAlign: "start", paddingLeft: "2rem" }}
          >
            <InputLabel id="demo-simple-select-label">Department</InputLabel>
          </div>
          <div
            style={{ width: "30%", textAlign: "start", paddingLeft: "2rem" }}
          >
            <Select
              id="demo-simple-select"
              value={department}
              onChange={this.handleChange}
            >
              {this.props.appData.department_data.map((depart) => {
                if (depart._id !== localStorage.getItem("departmentId")) {
                  return (
                    <MenuItem value={depart._id} name={depart.departmentName}>
                      {depart.departmentName}
                    </MenuItem>
                  );
                }
                return null;
              })}
            </Select>
          </div>
        </Grid>
        <Grid
          container
          spacing={3}
          justify="space-around"
          alignItems="flex-start"
          style={styles.grid}
        >
          <div
            style={{ width: "30%", textAlign: "start", paddingLeft: "2rem" }}
          >
            <InputLabel id="demo-simple-select-label">UserList</InputLabel>
          </div>
          <div
            style={{ width: "30%", textAlign: "start", paddingLeft: "2rem" }}
          >
            <Select
              id="demo-simple-select"
              value={user}
              onChange={this.handleChangeUser}
            >
              {this.state.department === "" &&
                this.props.appData.userRegistration_data.map((userData) => (
                  <MenuItem value={userData._id} name={userData.username}>
                    {userData.username}
                  </MenuItem>
                ))}
              {this.state.department !== "" &&
                this.state.userArray.map((userData) => (
                  <MenuItem value={userData.username}>
                    {userData.username}
                  </MenuItem>
                ))}
            </Select>
          </div>
        </Grid>
        <Grid>
          <TextField
            id="outlined-basic"
            multiline
            rows={4}
            label="Message"
            variant="outlined"
            value={this.state.message}
            onChange={this.handleTextChange}
            style={styles.TextField}
          />
        </Grid>
        <Grid>
          <Button
            color="inherit"
            variant="outlined"
            style={{
              width: "6rem",
              marginTop: "1rem",
              backgroundColor: "#acd8c4",
              padding: "8px 1rem",
            }}
            onClick={this.handleSubmit}
          >
            Submit
          </Button>
        </Grid>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    appData: state.appData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    get_department_and_users_action: () =>
      dispatch(get_department_and_users_action()),
    create_form_action: (data) => dispatch(create_form_action(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormComponent);
