let {
  UserRegistrationModel,
  LoginValidate,
  SignUpValidate,
  UserDataValidate,
} = require("../models/userRegistration");
let Joi = require("joi");
const bcrypt = require("bcrypt");
const saltRounds = 10;
const fs = require("fs");
const jwt = require("jsonwebtoken");
const path = require("path");
const { departmentModel } = require("../models/department");
// let Winston = require('../winston');
// let logger = Winston.logger;

var privateKEY = fs.readFileSync(path.resolve("src/private.key"), "utf8");

exports.signup = async function (req, res, next) {
  try {
    let data = req.body;
    let usernameCheck = await UserRegistrationModel.findOne({
      username: data.username,
    });
    if (usernameCheck !== null) {
      throw { message: "username already exits", statusCode: "400" };
    }
    let emailCheck = await UserRegistrationModel.findOne({ email: data.email });
    if (emailCheck !== null) {
      throw { message: "email already exits", statusCode: "400" };
    }
    let result = Joi.validate(data, SignUpValidate);
    if (result.error !== null) {
      throw new Error(`Validation error occured ${result.error}`);
    }
    const myPlaintextPassword = data.password;

    const hashCode = await bcrypt.hash(myPlaintextPassword, saltRounds);
    let departments = await departmentModel.find({});
    let departmentCreated = "";
    let department_id = "";
    let allUsers = "";
    //
    console.log("user created ", departments);

    if (departments.length < 2) {
      departmentCreated = await new departmentModel({
        departmentName: "department " + (departments.length + 1),
        // users: [],
        created_at: new Date(),
      }).save();
      department_id = departmentCreated._id;
    } else {
      allUsers = await UserRegistrationModel.find({});
      if (allUsers.length % 2 === 1) {
        department_id = departments[1]._id;
      } else {
        department_id = departments[0]._id;
      }
    }

    // Store hash in your password DB.
    let userDetails = await new UserRegistrationModel({
      username: data.username,
      email: data.email,
      password: hashCode,
      department_id,
    }).save();
    const username = data.username;
    let payload = {
      username,
    };
    console.log(userDetails["_id"]);
    let signOptions = {
      expiresIn: "12h",
      algorithm: "RS256",
    };
    const token = jwt.sign(payload, privateKEY, signOptions);
    res.header("x-auth", token);
    // window.sessionStorage.setItem('token',token)
    const details = {
      userId: userDetails["_id"],
      token: token,
      department_id: userDetails.department_id,
    };
    res.send(details).status("200");
    // logger.info("successfully signed up to the server");
  } catch (err) {
    next(err);
  }
};

exports.login = async function (req, res, next) {
  try {
    let receivedData = req.body;
    console.log("asddddddddddddsdarefdf", receivedData);
    let result = Joi.validate(receivedData, LoginValidate);
    if (result.error !== null) {
      throw new Error(`Validation error occured ${result.error}`);
    }
    let storedData = await UserRegistrationModel.findOne({
      username: receivedData.username,
    });
    console.log("sadadsffffffffffffffffffffffff", storedData);
    if (storedData.username == receivedData.username) {
      const myPlaintextPassword = storedData.password;
      console.log("asdsfdfffffffffffffffffffffff", myPlaintextPassword);
      let result = bcrypt.compareSync(
        receivedData.password,
        myPlaintextPassword
      );
      if (result) {
        const username = receivedData.username;
        console.log(username);
        let payload = {
          username,
        };
        let signOptions = {
          expiresIn: "12h",
          algorithm: "RS256",
        };
        let token = jwt.sign(payload, privateKEY, signOptions);
        res.header("x-auth", token);
        console.log(token);
        // window.sessionStorag.setItem('token',token)
        console.log(storedData["_id"]);
        const details = {
          userId: storedData["_id"],
          token: token,
          email: storedData.email,
          department_id: storedData.department_id,
        };
        res.send(details).status("200");
        // logger.info("successfully logged in to the server");
      }
    }
  } catch (err) {
    next(err);
  }
};

exports.list_all_users = async function (req, res, next) {
  try {
    let resData = await UserRegistrationModel.find({})
      .populate("attended")
      .populate("attending")
      .populate("hosted")
      .populate("hosting")
      .select("-__v");
    if (resData === null || resData.length === 0) {
      let error = new Error();
      error.statusCode = 400;
      error.message = "No users found";
      next(error);
    } else {
      res.send(resData).status(200);
      //   logger.info('displayed all the users in database');
    }
  } catch (err) {
    next(err);
  }
};

exports.read_a_user = async function (req, res, next) {
  try {
    let resData = await UserRegistrationModel.findById(req.params.userId)
      .populate("attended")
      .populate("attending")
      .populate("hosted")
      .populate("hosting")
      .select("-__v");
    if (resData === null || resData.length === 0) {
      let error = new Error();
      error.statusCode = 400;
      error.message = "No such user found";
      next(error);
    } else {
      res.send(resData).status(200);
      // logger.info('displayed a user from database');
    }
  } catch (err) {
    next(err);
  }
};

exports.update_a_user = async function (req, res, next) {
  try {
    console.log(req.body);
    let result = Joi.validate(req.body, UserDataValidate);
    // if (result.error !== null) {
    //   throw new Error(`Validation error occured ${result.error}`);
    // }
    let user = await UserRegistrationModel.findById({ _id: req.params.userId });
    if (user === null) {
      let error = new Error();
      error.statusCode = 400;
      error.message = "No such user found";
      next(error);
    } else {
      let updatedUser = await UserRegistrationModel.findOneAndUpdate(
        { _id: req.params.userId },
        { $set: req.body },
        { new: true }
      );
      res.send(updatedUser).status(200);
      //   logger.info('updated a user in database');
    }
  } catch (err) {
    next(err);
  }
};
