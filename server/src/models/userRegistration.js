const mongoose = require("mongoose");
const Joi = require("joi");
const Schema = mongoose.Schema;

let UserRegistrationSchema = new Schema({
  username: {
    type: String,
    trim: true,
    required: true,
    unique: true,
  },
  email: { type: String, trim: true, required: true, unique: true },
  password: { type: String, trim: true, required: true },
  department_id: { type: Schema.Types.ObjectId, ref: "departmentModel" },
});

module.exports.UserRegistrationModel = mongoose.model(
  "UserRegistrationModel",
  UserRegistrationSchema
);

module.exports.SignUpValidate = Joi.object().keys({
  username: Joi.string().alphanum().min(3).max(30).required(),
  email: Joi.string().email({ minDomainAtoms: 2 }).required(),
  password: Joi.string()
    .regex(/^[a-zA-Z0-9]{3,30}$/)
    .required(),
});

module.exports.LoginValidate = Joi.object().keys({
  username: Joi.string().alphanum().min(3).max(30).required(),
  password: Joi.string()
    .regex(/^[a-zA-Z0-9]{3,30}$/)
    .required(),
});

// module.exports.UserDataValidate = Joi.object().keys({
//   firstname: Joi.string().alphanum().min(3).max(15).required(),
//   lastname: Joi.string().alphanum().min(3).max(15).required(),
//   age: Joi.number().integer().min(10).max(60).required(),
//   avatar: Joi.string().required(),
//   gender: Joi.string().required(),
//   phone: Joi.string().min(10).max(10).required(),
//   location: Joi.string().required(),
//   bio: Joi.string().required(),
//   interest: Joi.string().required(),
// });
