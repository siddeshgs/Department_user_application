// const mongoose = require("mongoose");
// // let userData = require("../scripts/userData.json");
// let { UserRegistrationModel } = require("../models/userRegistration");
// // let eventData = require("../scripts/eventData.json");
// let { EventModel } = require("../models/event");
// let labelData = require("../scripts/labelData.json");
// let { LabelModel } = require("../models/label");
// let replyData = require("../scripts/replyData");
// let { ReplyModel } = require("../models/reply");
// // require("dotenv").config({ path: "src/.env" });
// // let mongoDB = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/department_user_apply`;
// // mongoose.connect(mongoDB, { useNewUrlParser: true });

// // let db = mongoose.connection;

// async function userList(data) {
//   // await UserRegistrationModel.collection.drop();
//   data.forEach(
//     async (element) =>
//       await new UserRegistrationModel({
//         username: element.username,
//         password: element.password,
//         email: element.email,
//         firstname: element.firstname,
//         lastname: element.lastname,
//         age: element.age,
//         gender: element.gender,
//         phone: element.phone,
//         location: element.location,
//         bio: element.bio,
//         interest: element.interest,
//         attended: element.attended,
//         attending: element.attending,
//         hosted: element.hosted,
//         hosting: element.hosting,
//       }).save()
//   );
// }

// // userList(userData);

// async function eventList(data) {
//   // await EventModel.collection.drop();
//   data.forEach(async (element) => {
//     let array = [];
//     let comments = new Object();
//     element.comments.forEach((comment) => {
//       comments = {
//         username: comment.username,
//         body: comment.body,
//         date: new Date(comment.date),
//         likes: comment.likes,
//         dislikes: comment.dislikes,
//         replies: comment.replies,
//       };
//       array.push(comments);
//     });
//     return await new EventModel({
//       img: element.img,
//       title: element.title,
//       author: element.author,
//       date: new Date(element.date),
//       labels: element.labels,
//       category: element.category,
//       description: element.description,
//       attendees: element.attendees,
//       comments: array,
//     }).save();
//   });
// }

// // eventList(eventData);

// async function labelList(data) {
//   // await LabelModel.collection.drop();
//   data.forEach(
//     async (element) =>
//       await new LabelModel({
//         name: element.name,
//       }).save()
//   );
// }

// // labelList(labelData);

// async function replyList(data) {
//   // await ReplyModel.collection.drop();
//   data.forEach(
//     async (element) =>
//       await new ReplyModel({
//         username: element.username,
//         body: element.body,
//       }).save()
//   );
// }

// // replyList(replyData);

// db.on("error", console.error.bind(console, "MongoDB connection error:"));
