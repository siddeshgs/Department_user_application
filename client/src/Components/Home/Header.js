import React, { Component } from "react";
import {
  AppBar,
  Toolbar,
  withTheme,
  Button,
  IconButton,
} from "@material-ui/core";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Card from "@material-ui/core/Card";
import FormComponent from "../Form/form";
import PendingForm from "../Form/pendingForm";
import ApprovedForm from "../Form/approvedForm";
import RequestForApprovalForm from "../Form/requestForApprovalForm";
import socket from "../../socket";

class Header extends Component {
  state = {
    value: 0,
    notify: false,
  };

  componentDidMount = () => {
    // if(this.state.notify!=)
    // socket.on("new request form", (data) => {
    //   console.log(data);
    //   if (data) {
    //     this.setState({ notify: true });
    //   }
    // });
    socket.on("connect", () => {
      console.log("connected");
    });
    socket.on("new request form", (data) => {
      console.log(data);
    });
  };

  componentDidUpdate = () => {
    socket.on("new request form", (data) => {
      console.log(data);
    });
  };

  render() {
    const { theme } = this.props;
    const styles = {
      appbar: {
        background: theme.palette.primary.main,
        position: "fixed",
        top: 0,
      },
      button: {
        background: theme.palette.secondary.main,
        margin: "0 1rem 0 1rem",
      },
      toolbar: {
        justifyContent: "space-between",
      },
      icon: {
        color: theme.palette.secondary.main,
        fontSize: "2rem",
      },
      modal: {
        margin: "5rem",
        // width: "10rem",
        textAlign: "center",
      },
      card: {
        margin: "5rem",
        textAlign: "center",
        minHeight: "30rem",
        maxWidth: "50%",
        margin: "0 auto",
      },
    };

    // console.log(this.props, this.state);

    const TabPanel = (props) => {
      // console.log("props", props);
      const { children, value, index, ...other } = props;

      return (
        <div
          role="tabpanel"
          hidden={value !== index}
          id={`simple-tabpanel-${index}`}
          aria-labelledby={`simple-tab-${index}`}
          {...other}
        >
          {value === index && <Card style={styles.card}>{children}</Card>}
        </div>
      );
    };

    TabPanel.propTypes = {
      children: PropTypes.node,
      index: PropTypes.any.isRequired,
      value: PropTypes.any.isRequired,
    };

    const handleChange = (event, newValue) => {
      this.setState({ value: newValue });
    };

    const a11yProps = (index) => {
      return {
        id: `simple-tab-${index}`,
        "aria-controls": `simple-tabpanel-${index}`,
      };
    };
    console.log("here props", this.props.location.pathname);

    return (
      <>
        <AppBar style={styles.appbar} position="static">
          {this.props.location.pathname !== "/switchon/signup" &&
          this.props.location.pathname !== "/switchon/login" ? (
            <Toolbar>
              <Tabs
                value={this.state.value}
                onChange={handleChange}
                aria-label="simple tabs example"
              >
                <Tab label="Form" {...a11yProps(0)} />
                <Tab label="Pending" {...a11yProps(1)} />
                <Tab label="Approved" {...a11yProps(2)} />
                <Tab label="Request(For Approval)" {...a11yProps(3)} />
              </Tabs>
              <div style={{ position: "absolute", right: "10px" }}>
                <Button color="inherit" style={{ marginRight: "8px" }}>
                  {localStorage.getItem("username")}
                </Button>
                {/* <IconButton
                  color="inherit"
                  onClick={() => alert("clicked notification icon")}
                >
                  <NotificationsIcon />
                </IconButton> */}
                <Button
                  color="inherit"
                  onClick={() => {
                    sessionStorage.clear();
                    localStorage.clear();
                    this.props.history.push("/switchon/login");
                  }}
                >
                  {"Logout"}
                </Button>
              </div>
            </Toolbar>
          ) : (
            <Toolbar>
              <div>SWITCH ON</div>
              <div style={{ position: "absolute", right: "10px" }}>
                {this.props.location.pathname === "/switchon/signup" ? (
                  <Button
                    color="inherit"
                    onClick={() => {
                      this.props.history.push("/switchon/login");
                    }}
                  >
                    Login
                  </Button>
                ) : (
                  <Button
                    color="inherit"
                    onClick={() => {
                      this.props.history.push("/switchon/signup");
                    }}
                  >
                    Signup
                  </Button>
                )}
              </div>
            </Toolbar>
          )}
        </AppBar>
        {this.props.location.pathname !== "/switchon/signup" &&
        this.props.location.pathname !== "/switchon/login" ? (
          <div style={styles.modal}>
            <TabPanel value={this.state.value} index={0}>
              <FormComponent
                history={this.props.history}
                location={this.props.location}
              />
            </TabPanel>
            <TabPanel value={this.state.value} index={1}>
              <PendingForm
                history={this.props.history}
                location={this.props.location}
              />
            </TabPanel>
            <TabPanel value={this.state.value} index={2}>
              <ApprovedForm
                history={this.props.history}
                location={this.props.location}
              />
            </TabPanel>
            <TabPanel value={this.state.value} index={3}>
              <RequestForApprovalForm
                history={this.props.history}
                location={this.props.location}
              />
            </TabPanel>
          </div>
        ) : null}
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    theme: state.themes.theme,
  };
};
export default connect(mapStateToProps, null)(withTheme()(Header));
