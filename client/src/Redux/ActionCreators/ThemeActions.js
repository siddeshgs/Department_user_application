import { SET_THEME } from "./ActionTypes";

export const setTheme = theme => ({
  type: SET_THEME,
  theme
});
