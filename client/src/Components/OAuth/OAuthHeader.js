import React, { Component } from "react";
import { NavLink } from "react-router-dom";
class OAuthHeader extends Component {
  state = {};
  render() {
    return (
      <div className="SignupHeader">
        <h2>Join switchon.</h2>
        <NavLink to="/switchon/">Home</NavLink>
        <p>
          Create an account to recieve great events in your inbox,to share
          events and attend events.
        </p>
      </div>
    );
  }
}

export default OAuthHeader;
