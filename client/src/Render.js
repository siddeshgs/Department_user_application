import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import {
  MuiThemeProvider,
  createMuiTheme,
  withTheme,
} from "@material-ui/core/styles";
import UserHome from "./Components/Home/Header";
import Login from "./Components/Login/Login";
import Email from "./Components/Email/Email";
import Error from "./Components/Error";
import { connect } from "react-redux";

import { setTheme } from "./Redux/ActionCreators/ThemeActions";

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: {
      main: "#222629",
    },
    secondary: {
      main: "#86C232",
    },
  },
});
class Render extends React.Component {
  render() {
    this.props.setTheme(theme);
    return (
      <BrowserRouter>
        <MuiThemeProvider theme={theme}>
          <Switch>
            <Route
              path="/"
              exact
              render={() => {
                return <Redirect to="/switchon/signup" />;
              }}
            />
            <Route path="/switchon/login" component={Login} exact strict />
            <Route path="/switchon/signup" component={Email} exact />
            <Route path="/switchon/home/:username" component={UserHome} />
            <Route component={Error} />
          </Switch>
        </MuiThemeProvider>
      </BrowserRouter>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    setTheme: (theme) => dispatch(setTheme(theme)),
  };
};

export default connect(null, mapDispatchToProps)(withTheme()(Render));
