const router = require("./router");
const userList = require("../controllers/userRegistrationController");

// user Routes
router.post("/api/switchon/signup", userList.signup);
router.post("/api/switchon/login", userList.login);
router.get("/api/switchon/users", userList.list_all_users);

router.get("/api/switchon/users/:userId", userList.read_a_user);
router.put("/api/switchon/users/:userId", userList.update_a_user);
module.exports = router;
