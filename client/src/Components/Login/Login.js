import React, { Component } from "react";
import { NavLink, Redirect } from "react-router-dom";
import axios from "axios";
import { connect } from "react-redux";
import { withTheme, Button, InputLabel, Input } from "@material-ui/core";
import { authentication } from "../UserAuthentication/authentication";
import "./Login.css";
import Header from "../Home/Header";
import ENV from "../../environment";

class Login extends Component {
  state = {
    username: "",
    email: "",
    token: null,
    loggedIn: false,
    userId: null,
    department_id: null,
  };

  handleUsernameChange = (value) => {
    this.setState({
      username: value,
    });
  };

  handlePasswordChange = (value) => {
    this.setState({
      password: value,
    });
  };

  handleOnSubmit = (e, username, password) => {
    e.preventDefault();
    // console.log(username);
    const url = `${ENV.API_URL}/api/switchon/login`;
    axios
      .post(url, {
        username,
        password,
      })
      .then((resp) => {
        // console.log(resp);
        authentication.authenticate(() => {
          localStorage.setItem("username", this.state.username);

          // console.log("here now ", resp);
          this.setState(
            {
              userId: resp.data.userId,
              email: resp.data.email,
              loggedIn: true,
              token: resp.data.token,
              department_id: resp.data.department_id,
            },
            () => console.log(this.state)
          );
        });
      })
      .catch((err) => console.error(err));
  };

  componentDidMount() {}
  render() {
    // console.log(this.props)
    const { from } = this.props.location.state || {
      from: { pathname: `/switchon/home/${this.state.username}` },
    };
    const { loggedIn } = this.state;
    if (loggedIn) {
      sessionStorage.setItem("token", this.state.token);
      localStorage.setItem("email", this.state.email);
      localStorage.setItem("userId", this.state.userId);
      localStorage.setItem("departmentId", this.state.department_id);

      return <Redirect to={from} />;
    }

    const styles = {
      button: {
        background: this.props.getTheme.palette.secondary.main,
        margin: "2rem",
        width: "15rem",
      },
      inputLabel: {
        marginRight: "2rem",
      },
    };
    return (
      <React.Fragment>
        <Header history={this.props.history} location={this.props.location} />
        <form
          style={{
            position: "absolute",
            left: "50%",
            top: "50%",
            transform: "translate(-50%,-50%)",
          }}
          method="GET"
        >
          <div style={{ marginBottom: "1rem" }}>
            <InputLabel style={styles.inputLabel}>Username</InputLabel>
            <Input
              onChange={(e) => this.handleUsernameChange(e.target.value)}
            />
          </div>
          <div>
            <InputLabel style={styles.inputLabel}>Password</InputLabel>
            <Input
              type="password"
              onChange={(e) => this.handlePasswordChange(e.target.value)}
            />
          </div>
          <Button
            onClick={(e) =>
              this.handleOnSubmit(e, this.state.username, this.state.password)
            }
            style={styles.button}
            type="submit"
          >
            Login
          </Button>
          <p style={{ textAlign: "center" }}>
            No account?
            <NavLink to="signup">Create one</NavLink>
          </p>
        </form>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    getTheme: state.themes.theme,
  };
};
export default connect(mapStateToProps, null)(withTheme()(Login));
